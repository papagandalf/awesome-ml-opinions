# machine learning opinion papers [![Awesome](https://awesome.re/badge-flat2.svg)](https://awesome.re)

List of mainly opinion papers (and other resources). 
I find that such papers are a very good starting point for (reading) group 
discussions, especially if said group members already  take part in other 
reading groups and do not have much time to read yet another technical paper.

Some of the papers listed here might not be strictly speaking "opinion papers",
but they surely are on the not-so-technical side.


## general

* Forde, J. F., Paganini, M. 2019. *The Scientific Method in the Science of Machine Learning* [[pdf](https://scontent-amt2-1.xx.fbcdn.net/v/t39.8562-6/78999983_507801233151300_8895130161985355776_n.pdf?_nc_cat=109&_nc_oc=AQkYE8Fh3WtamEBWlOmEGdFuSgRb8aZ0yh8OdY_LK8d6B4OLgar9s0eOT6FwrL0CyK8&_nc_ht=scontent-amt2-1.xx&oh=3848b0af4404efa0466f2f952b038dab&oe=5ED73656)]
* Darwiche, A. 2017. *Human-Level Intelligence or Animal-Like Abilities?* [[pdf](https://arxiv.org/pdf/1707.04327.pdf)]


## debates

* *Norvig vs. Chomsky and the Fight for the Future of AI* [[link](https://www.tor.com/2011/06/21/norvig-vs-chomsky-and-the-fight-for-the-future-of-ai/)]
* *Bengio vs Marcus debate* [[video](https://www.youtube.com/watch?v=EeqwFjqFvJA&feature=emb_title)] [[transcript](https://medium.com/@Montreal.AI/transcript-of-the-ai-debate-1e098eeb8465)] [[Bengio's slides](https://montrealartificialintelligence.com/aidebate/slidesbengio.pdf)] [[Marcus' slides](https://montrealartificialintelligence.com/aidebate/slidesmarcus.pdf)]
## reviewing

* Church, K. 2006. *Reviewing the Reviewers* [[pdf](https://www.mitpressjournals.org/doi/pdf/10.1162/089120105775299131)]

## reproducibility


## interpretability 

* Lipton, Z. 2006. *The Mythos of Model Interpretability* [[pdf](https://arxiv.org/abs/1606.03490)]

## NLP
